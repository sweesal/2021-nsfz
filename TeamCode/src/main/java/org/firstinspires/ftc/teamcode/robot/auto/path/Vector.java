package org.firstinspires.ftc.teamcode.robot.auto.path;

// Actually a 1 * 2 matrix, may be deprecated later.
public class Vector {
    public double dx;
    public double dy;

    public Vector() {
    }

    public Vector(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public Vector(Point p) {
        this.dx = p.x;
        this.dy = p.y;
    }

    public Vector(Point p1, Point p2) {
        this.dx = p2.x - p1.x;
        this.dy = p2.y - p1.y;
    }

    public String toString() {
        return "Vector(" + dx + ", " + dy + ")";
    }

    public double norm() {
        return Math.sqrt(dx * dx + dy * dy);
    }

    public Vector add(Vector input) {
        Vector v2 = new Vector(this.dx + input.dx, this.dy + input.dy);
        return v2;
    }

    public Vector subtract(Vector input) {
        Vector v2 = new Vector(this.dx - input.dx, this.dy - input.dy);
        return v2;
    }

    public Vector scale(double factor) {
        return new Vector(this.dx * factor, this.dy * factor);
    }

    public Vector normalize() {
        double length = norm();
        if (length != 0) {
            return new Vector(this.dx / length, this.dy / length);
        }
        return new Vector();
    }

    public double dotProduct(Vector v) {
        return this.dx * v.dx + this.dy * v.dy;
    }

}
