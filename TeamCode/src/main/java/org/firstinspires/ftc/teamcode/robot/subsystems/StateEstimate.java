package org.firstinspires.ftc.teamcode.robot.subsystems;

import org.firstinspires.ftc.teamcode.lib.geometry.Rotation2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Twist2d;
import org.firstinspires.ftc.teamcode.lib.kinematics.TrackingWheelKinematics;
import org.firstinspires.ftc.teamcode.robot.Constants;

public class StateEstimate {
    private DriveTrain driveTrain = DriveTrain.getInstance();
    private double left_encoder_prev_distance_ = 0.0;
    private double right_encoder_prev_distance_ = 0.0;
    private double prev_timestamp_ = -1.0;
    private Rotation2d prev_heading_ = null;
    private TrackingWheelKinematics kinematics;

    private StateEstimate () {
        kinematics = new TrackingWheelKinematics(Constants.ROBOT_T_WIDTH, Constants.X_TRK_WHL_Y_OFFSET);
        left_encoder_prev_distance_ = driveTrain.getLeftPosition();
        right_encoder_prev_distance_ = driveTrain.getRightPosition();
    }

    public void update(double timestamp) {
        if (prev_heading_ == null) {
            prev_heading_ = new Rotation2d(0, false);
        }
        final double dt = timestamp - prev_timestamp_;
        final double left_distance = driveTrain.getLeftPosition();
        final double right_distance = driveTrain.getRightPosition();
        final double delta_left = left_distance - left_encoder_prev_distance_;
        final double delta_right = right_distance - right_encoder_prev_distance_;
        final Rotation2d gyro_angle = driveTrain.getHeading();
        Twist2d odometry_twist;
//        synchronized (mRobotState) {
//            final Pose2d last_measurement = mRobotState.getLatestFieldToVehicle().getValue();
//            odometry_twist = kinematics.forwardKinematics(last_measurement.getRotation(), delta_left,
//                    delta_right, gyro_angle);
//        }
//        final Twist2d measured_velocity = kinematics.forwardKinematics(
//                delta_left, delta_right, prev_heading_.inverse().rotateBy(gyro_angle).getRadians()).scaled(1.0 / dt);
//        final Twist2d predicted_velocity = kinematics.forwardKinematics(driveTrain.getLeftVelocity(),
//                mDrive.getRightLinearVelocity()).scaled(dt);
//        mRobotState.addVehicleToTurretObservation(timestamp,
//                Rotation2d.fromDegrees(Turret.getInstance().getAngle()));
//        mRobotState.addObservations(timestamp, odometry_twist, measured_velocity,
//                predicted_velocity);
        left_encoder_prev_distance_ = left_distance;
        right_encoder_prev_distance_ = right_distance;
        prev_heading_ = gyro_angle;
        prev_timestamp_ = timestamp;
    }

}
