/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.lib.util.ButtonPress;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.auto.AutoTest;
import org.firstinspires.ftc.teamcode.robot.subsystems.DriveTrain;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="Two Controllers", group="Manual")
//@Disabled

public class TwoDriverBotLinear extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();
    private DriveTrain driveTrain;
    private AutoTest auto;

    private Intake intake;
    private Shooter shooter;

    enum Mode {
        DRIVER_CONTROL,
        AUTOMATIC_CONTROL
    }

    Mode currentMode = Mode.DRIVER_CONTROL;

    @Override
    public void runOpMode() throws InterruptedException {
        RobotMap.robotInit(hardwareMap);
        driveTrain = DriveTrain.getInstance();
        auto = AutoTest.getInstance();

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        Intake.setIsIntakeTriggered(false);
        driveTrain.init();
        telemetry.addData("Status", "Initialized");
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        int SHOOTER_MAX = -2400;
        boolean isTargetShootingRequired = false;
        Pose2d initialPose = new Pose2d(0, 24, 0);
        Pose2d shootPose = new Pose2d(50, 24, Math.PI);
        Pose2d shootTargetPose = new Pose2d(65, 0, Math.PI);

        drive.update();
        drive.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        drive.setPoseEstimate(initialPose);

        Pose2d poseShootTarget;

        waitForStart();

        if (isStopRequested()) return;

        while (opModeIsActive() && !isStopRequested()) {

            drive.update();
            Pose2d poseEstimate = drive.getPoseEstimate();

            ButtonPress.giveMeInputsForTwo(gamepad1.a, gamepad1.b, gamepad1.x, gamepad1.y,
                    gamepad1.dpad_up, gamepad1.dpad_down, gamepad1.dpad_right, gamepad1.dpad_left,
                    gamepad1.right_bumper, gamepad1.left_bumper,
                    gamepad1.left_stick_button, gamepad1.right_stick_button,
                    gamepad1.left_trigger > 0.5, gamepad1.right_trigger > 0.5,

                    gamepad2.a, gamepad2.b, gamepad2.x, gamepad2.y,
                    gamepad2.dpad_up, gamepad2.dpad_down, gamepad2.dpad_right, gamepad2.dpad_left,
                    gamepad2.right_bumper, gamepad2.left_bumper,
                    gamepad2.left_stick_button, gamepad2.right_stick_button,
                    gamepad2.left_trigger > 0.5, gamepad2.right_trigger > 0.5
            );

            shooter.setShooterVelocity(SHOOTER_MAX, ButtonPress.isGamepad2_left_trigger_pressed());
            shooter.updateTriggerRoller(gamepad2.right_trigger > 0.5);
            shooter.updateSlope(
                    ButtonPress.isGamepad2_dpad_up_pressed(), ButtonPress.isGamepad2_dpad_down_pressed(), isTargetShootingRequired, Shooter.isIsShooting());
            intake.updateIntake(ButtonPress.isGamepad2_left_bumper_pressed(), gamepad2.right_bumper);


            if (ButtonPress.isGamepad1_left_bumper_pressed()) {
                drive.setPoseEstimate(shootPose);
            }

            if (ButtonPress.isGamepad1_right_bumper_pressed()) {
                Pose2d rightSidePose = new Pose2d(poseEstimate.getX(), 0, poseEstimate.getHeading());
                drive.setPoseEstimate(rightSidePose);
            }


            switch (currentMode) {
                case DRIVER_CONTROL:
                    if (gamepad1.dpad_left) driveTrain.resetOdometer();
                    if (gamepad1.a) {
                        auto.goTurnAngleTo(180);
                    } else if (gamepad1.b) {
                        auto.goTurnAngleTo(0);
                    } else if (gamepad1.dpad_left | gamepad1.dpad_right) {
                        driveTrain.driveStrafe(
                                gamepad1.dpad_left, gamepad1.dpad_right, shooter.getShooterVelocity() < -1500);
                    } else if (gamepad1.dpad_up | gamepad1.dpad_down) {
                        driveTrain.driveStraight(
                                gamepad1.dpad_up, gamepad1.dpad_down, shooter.getShooterVelocity() < -1500);
                    } else {
                        auto.setIdle();
                        driveTrain.driveMecanumWithCurvature(gamepad1.left_stick_x, gamepad1.right_stick_x, -gamepad1.left_stick_y,
                                gamepad1.x, ButtonPress.isGamepad1_y_pressed());
                    }
                    if (gamepad1.right_stick_button) {
                        Trajectory traj1 = drive.trajectoryBuilder(poseEstimate)
                                .lineToLinearHeading(shootPose)
                                .addTemporalMarker(1, () -> {
                                    Shooter.setIsShooting(true);
                                    shooter.setShooterVelocityForceRun(SHOOTER_MAX);
                                    shooter.setSlopeState(Shooter.SlopeState.TOP);
                                })
                                .build();
                        drive.followTrajectoryAsync(traj1);
                        currentMode = Mode.AUTOMATIC_CONTROL;
                    }
                    break;

                case AUTOMATIC_CONTROL:
                    if (gamepad1.left_stick_button) {
                        drive.waitForIdle();
                        currentMode = Mode.DRIVER_CONTROL;
                    }
                    if (!drive.isBusy()) {
                        currentMode = Mode.DRIVER_CONTROL;
                    }
                    break;
            }

            telemetry.addData("Robot pose estimate", poseEstimate.toString());
            telemetry.addData("Drive state", currentMode);
            telemetry.addData("Trigger state", shooter.getTriggerState());
            telemetry.addData("Ring shot", shooter.ringExceedUpdate());
            telemetry.addData("Turret pitch", shooter.getSlope());
            telemetry.addData("Slope state", shooter.getSlopeState());
            telemetry.addData("Flywheel velocity", shooter.getShooterVelocity());
            telemetry.addData("Heading in deg", driveTrain.getHeading().toString());
            telemetry.addData("Robot pose", driveTrain.printPose());
            telemetry.addData("Field relative", driveTrain.getIsFieldRelative());
            telemetry.addData("Status", "Run time: " + runtime.toString());

            telemetry.update();
        }


        if (isStopRequested()) {
            driveTrain.resetOdometer();
            driveTrain.resetEncoder();
        }

    }

}
