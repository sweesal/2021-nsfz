package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

public class RobotMap {

    public static DcMotorEx leftFront = null;
    public static DcMotorEx leftRear = null;
    public static DcMotorEx rightFront = null;
    public static DcMotorEx rightRear = null;
    public static DcMotorEx shooterLeft = null;
    public static DcMotorEx shooterRight = null;
    public static DcMotorEx intake = null;

    public static DcMotorEx virtualLeft = null;

    public static DigitalChannel switchShoot = null;

    public static Servo triggerRoller = null;
    public static Servo slope = null;

    public static AnalogInput potentiometer = null;

    public static BNO055IMU imu;

    private static final String TFOD_MODEL_ASSET = "UltimateGoal.tflite";
    private static final String LABEL_FIRST_ELEMENT = "Quad";
    private static final String LABEL_SECOND_ELEMENT = "Single";

    private static final String VUFORIA_KEY =
            "Ab2/g1D/////AAABmYvdhku27UGVn935qTk8e26JpZ0ZXgvvhZtYQoH4+yyY0ZgPKp1lL5ML6Bqc" +
                    "kDViMfPpXoyj4UI0sx/XOPALtsS4AMmifoqpv6CjtsTjAJ/FKEoKmouP5xyOyINQ8Sey" +
                    "3f/sRnABbaMUCKt0A9zGwSmoFaAWuHbPLShczMPN2YVMqXWEdv/DMpcdsyceYO7Ch0BA" +
                    "78ZxdRTcvgdD51lYlW+lWbpqhLOOSQ8U2QD9QRH/EUU0fI1hOLChznSU932fGISealuM" +
                    "9CxzV38Lwpo6f4ki2Py3P+Z7PIuhtZVLDBqkvtxSiZdXxXfCqkFg/rw88xY4HXxj3Yxt" +
                    "lQ0b0uk/EK6GQmpgv7+ewEP43svGmpzR";

    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;
    private int bagelNum = 0;

    public static void robotInit(HardwareMap hardwareMap){
        leftFront = hardwareMap.get(DcMotorEx.class,"leftfront");
        leftRear = hardwareMap.get(DcMotorEx.class,"leftrear");
        rightFront = hardwareMap.get(DcMotorEx.class,"rightfront");
        rightRear = hardwareMap.get(DcMotorEx.class,"rightrear");


        intake = hardwareMap.get(DcMotorEx.class,"intake");

        shooterLeft = hardwareMap.get(DcMotorEx.class,"shooter1");
        shooterRight = hardwareMap.get(DcMotorEx.class,"shooter2");

        virtualLeft = hardwareMap.get(DcMotorEx.class, "virtualleft");

        triggerRoller = hardwareMap.get(Servo.class, "triggerroller");
        slope = hardwareMap.get(Servo.class,"slope");
        potentiometer = hardwareMap.get(AnalogInput.class, "potentiometer");

        switchShoot = hardwareMap.get(DigitalChannel.class, "isshoot");

        imu = hardwareMap.get(BNO055IMU.class, "imu");
        BNO055IMU.Parameters imuParams = new BNO055IMU.Parameters();
        imu.initialize(imuParams);



//        VuforiaLocalizer.Parameters vufParams = new VuforiaLocalizer.Parameters();
//        vufParams.vuforiaLicenseKey = VUFORIA_KEY;
//        vufParams.cameraName = hardwareMap.get(WebcamName.class, "Webcam 1");
//        vuforia = ClassFactory.getInstance().createVuforia(vufParams);
//
//        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
//                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
//        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
//        tfodParameters.minResultConfidence = 0.8f;
//        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
//        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_FIRST_ELEMENT, LABEL_SECOND_ELEMENT);
//
//        if (tfod != null) {
//            tfod.activate();

            // The TensorFlow software will scale the input images from the camera to a lower resolution.
            // This can result in lower detection accuracy at longer distances (> 55cm or 22").
            // If your target is at distance greater than 50 cm (20") you can adjust the magnification value
            // to artificially zoom in to the center of image.  For best results, the "aspectRatio" argument
            // should be set to the value of the images used to create the TensorFlow Object Detection model
            // (typically 16/9).
//            tfod.setZoom(3, 4.0/3.0);
//        }
    }

    public void initDcMotor(DcMotor motor){
        motor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public int identify () {
        return 0;
    }


}
