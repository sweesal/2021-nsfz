package org.firstinspires.ftc.teamcode.lib.kinematics;


import org.ejml.simple.SimpleMatrix;

public class MecanumKinematics {

    private SimpleMatrix fwdKinematics;
    private SimpleMatrix invKinematics;
    private SimpleMatrix wheelSpeedMatrix = new SimpleMatrix(4, 1); // 4*1 matrix
    private SimpleMatrix chassisSpeedMatrix = new SimpleMatrix(3, 1); // 3*1 matrix

    public MecanumKinematics() {}

    public MecanumKinematics(double robotLength, double robotWidth) {
        invKinematics = new SimpleMatrix(4, 3);
        setInvKinematics(robotLength/2, robotWidth/2); // make values half
        fwdKinematics = invKinematics.pseudoInverse();
    }

    private void setInvKinematics (double halfLength, double halfWidth) {
        double frameParam = halfLength + halfWidth;
        invKinematics.setRow(0, 0, 1, -1, frameParam);
        invKinematics.setRow(1, 0, 1,  1, -frameParam);
        invKinematics.setRow(2, 0, 1, -1, -frameParam);
        invKinematics.setRow(3, 0, 1,  1, frameParam);
    }

    private void setInvKinematics (double halfLength, double halfWidth, double rollerAngle) {
        // When roller angle != 45 degrees.
        // I'm lazy so I just leave it here.
    }

    public ChassisSpeeds toChassisSpeed (MecanumWheelSpeeds mecanumWheelSpeeds){
        wheelSpeedMatrix.setColumn(0, 0,
                mecanumWheelSpeeds.rightFrontMetersPerSecond, mecanumWheelSpeeds.leftFrontMetersPerSecond,
                mecanumWheelSpeeds.leftRearMetersPerSecond, mecanumWheelSpeeds.rightRearMetersPerSecond); // Anticlockwise.
        chassisSpeedMatrix = fwdKinematics.mult(wheelSpeedMatrix);
        return new ChassisSpeeds(
                chassisSpeedMatrix.get(0, 0),
                chassisSpeedMatrix.get(1, 0),
                chassisSpeedMatrix.get(2, 0)
        );
    }

    public MecanumWheelSpeeds toWheelSpeeds (ChassisSpeeds chassisSpeeds) {
        chassisSpeedMatrix.setColumn(0,0,
                chassisSpeeds.vy,
                chassisSpeeds.vx,
                chassisSpeeds.omega);
        wheelSpeedMatrix = invKinematics.mult(chassisSpeedMatrix);
        return new MecanumWheelSpeeds(
                wheelSpeedMatrix.get(0, 0),
                wheelSpeedMatrix.get(1, 0),
                wheelSpeedMatrix.get(2, 0),
                wheelSpeedMatrix.get(3, 0)
        );
    }


}
