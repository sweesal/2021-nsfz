package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.lib.geometry.Pose2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Rotation2d;
import org.firstinspires.ftc.teamcode.lib.kinematics.ChassisSpeeds;
import org.firstinspires.ftc.teamcode.lib.kinematics.MecanumKinematics;
import org.firstinspires.ftc.teamcode.lib.kinematics.MecanumWheelSpeeds;
import org.firstinspires.ftc.teamcode.lib.kinematics.Odometer;
import org.firstinspires.ftc.teamcode.lib.kinematics.TrackingWheelKinematics;
import org.firstinspires.ftc.teamcode.lib.kinematics.TrackingWheelSpeeds;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.auto.utilities.MyMath;

import java.util.ArrayList;
import java.util.List;

import static org.firstinspires.ftc.teamcode.robot.Constants.encoderTicksToMeters;

public class DriveTrain {

    private static final DriveTrain driveTrain = new DriveTrain();

    public static DriveTrain getInstance() {
        return driveTrain;
    }

    private final DcMotorEx leftFront = RobotMap.leftFront;
    private final DcMotorEx leftRear = RobotMap.leftRear;
    private final DcMotorEx rightFront = RobotMap.rightFront;
    private final DcMotorEx rightRear = RobotMap.rightRear;
    private DcMotorEx xPosEncoder = RobotMap.intake;
    private DcMotorEx leftTrackingWheel = RobotMap.virtualLeft;
    private DcMotorEx rightTrackingWheel = RobotMap.shooterRight;
    private final BNO055IMU imu = RobotMap.imu;
    private static boolean isFieldRelative = false;
    private MecanumKinematics mecanumKinematics;
    private TrackingWheelKinematics trackingWheelKinematics;
    private Odometer odometer;

    private DriveTrain() {
        leftFront.setMode(DcMotorEx.RunMode.RUN_WITHOUT_ENCODER);
        leftRear.setMode(DcMotorEx.RunMode.RUN_WITHOUT_ENCODER);
        rightFront.setMode(DcMotorEx.RunMode.RUN_WITHOUT_ENCODER);
        rightRear.setMode(DcMotorEx.RunMode.RUN_WITHOUT_ENCODER);

        //xPosEncoder.setMode(DcMotorEx.RunMode.RUN_WITHOUT_ENCODER);

        mecanumKinematics = new MecanumKinematics(Constants.ROBOT_T_LENGTH, Constants.ROBOT_T_WIDTH + 1.5);
        trackingWheelKinematics = new TrackingWheelKinematics(Constants.ROBOT_T_WIDTH, Constants.X_TRK_WHL_Y_OFFSET);

        odometer = new Odometer(trackingWheelKinematics, getHeading());
    }

    public void init () {
        leftFront.setDirection(DcMotorEx.Direction.REVERSE);
        leftRear.setDirection(DcMotorEx.Direction.REVERSE);
        rightFront.setDirection(DcMotorEx.Direction.FORWARD);
        rightRear.setDirection(DcMotorEx.Direction.FORWARD);
        leftFront.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        leftRear.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        rightFront.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        rightRear.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
    }

    public void resetEncoder () {
        leftFront.setMode(DcMotorEx.RunMode.STOP_AND_RESET_ENCODER);
        leftRear.setMode(DcMotorEx.RunMode.STOP_AND_RESET_ENCODER);
        rightFront.setMode(DcMotorEx.RunMode.STOP_AND_RESET_ENCODER);
        rightRear.setMode(DcMotorEx.RunMode.STOP_AND_RESET_ENCODER);

        //xPosEncoder.setMode(DcMotorEx.RunMode.STOP_AND_RESET_ENCODER);
    }

    public void driveMecanum(double xSpeed, double zRotation, double yTranslation) {
        double vx = applySimpleDeadBand(xSpeed) * Constants.getMaxSpeedPerSec();
        double vy = applySimpleDeadBand(yTranslation) * Constants.getMaxSpeedPerSec();
        MecanumWheelSpeeds mecanumWheelSpeeds = mecanumKinematics.toWheelSpeeds(isFieldRelative ? ChassisSpeeds.fromFieldRelativeSpeeds(
                vy, vx, zRotation, getHeading())
                : new ChassisSpeeds(vy, vx, zRotation));
        rightFront.setPower(mecanumWheelSpeeds.rightFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftFront.setPower(mecanumWheelSpeeds.leftFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftRear.setPower(mecanumWheelSpeeds.leftRearMetersPerSecond / Constants.getMaxSpeedPerSec());
        rightRear.setPower(mecanumWheelSpeeds.rightRearMetersPerSecond / Constants.getMaxSpeedPerSec());
    }

    public void driveMecanum(double xSpeed, double zRotation, double yTranslation,
                             boolean isPowerReduced, boolean btnFieldRelative) {
        double vx = applySimpleDeadBand(xSpeed) * Constants.getMaxSpeedPerSec();
        double vy = applySimpleDeadBand(yTranslation) * Constants.getMaxSpeedPerSec();
        double omega = -zRotation; // Notice this is for adapting to the gamepad!!!
        if (btnFieldRelative) isFieldRelative = !isFieldRelative;
        double vX = Math.abs(vy) > 0.15 ? 0 : vx;
        MecanumWheelSpeeds mecanumWheelSpeeds = mecanumKinematics.toWheelSpeeds(isFieldRelative ? ChassisSpeeds.fromFieldRelativeSpeeds(
                vy, vx, omega, getHeading())
                : new ChassisSpeeds(vy, vx, omega));
        rightFront.setPower(isPowerReduced ? mecanumWheelSpeeds.rightFrontMetersPerSecond * 0.35 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.rightFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftFront.setPower(isPowerReduced ? mecanumWheelSpeeds.leftFrontMetersPerSecond * 0.35 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.leftFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftRear.setPower(isPowerReduced ? mecanumWheelSpeeds.leftRearMetersPerSecond * 0.35 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.leftRearMetersPerSecond / Constants.getMaxSpeedPerSec());
        rightRear.setPower(isPowerReduced ? mecanumWheelSpeeds.rightRearMetersPerSecond * 0.35 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.rightRearMetersPerSecond / Constants.getMaxSpeedPerSec());
    }

    public void driveMecanumWithCurvature(double xSpeed, double zRotation, double yTranslation,
                             boolean isPowerReduced, boolean btnFieldRelative) {
        // Modified from team 254's cheesy drive.
        double vx = applySimpleDeadBand(xSpeed) * Constants.getMaxSpeedPerSec();
        double vy = applySimpleDeadBand(yTranslation) * Constants.getMaxSpeedPerSec();
        double vX = Math.abs(yTranslation) < 0.125 ? vx : 0;
        double omegaRaw = Math.pow(xSpeed, yTranslation) > 0.3 ? -zRotation  : -zRotation * 0.2;
        double omega = -zRotation; // Notice this is for adapting to the gamepad!!!
        if (btnFieldRelative) isFieldRelative = !isFieldRelative;
        final double kWheelGain = 0.225;
        final double kWheelNonlinearity = 0.05;
        final double denominator = Math.sin(Math.PI / 2.0 * kWheelNonlinearity);
        // Apply a sin function that's scaled to make it feel better.
        boolean isQuickTurn = Math.abs(vy) < 0.1;
        if (!isQuickTurn) {
            omega = Math.sin(Math.PI / 2.0 * kWheelNonlinearity * omega);
            omega = Math.sin(Math.PI / 2.0 * kWheelNonlinearity * omega);
            omega = omega / (denominator * denominator) * Math.abs(yTranslation);
        }
        omega *= kWheelGain;
        MecanumWheelSpeeds mecanumWheelSpeeds = mecanumKinematics.toWheelSpeeds(
                isFieldRelative ? ChassisSpeeds.fromFieldRelativeSpeeds(vy, vx, omegaRaw, getHeading())
                                : new ChassisSpeeds(vy, vx, omega));
        rightFront.setPower(isPowerReduced ? mecanumWheelSpeeds.rightFrontMetersPerSecond * 0.4 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.rightFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftFront.setPower(isPowerReduced ? mecanumWheelSpeeds.leftFrontMetersPerSecond * 0.4 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.leftFrontMetersPerSecond / Constants.getMaxSpeedPerSec());
        leftRear.setPower(isPowerReduced ? mecanumWheelSpeeds.leftRearMetersPerSecond * 0.4 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.leftRearMetersPerSecond / Constants.getMaxSpeedPerSec());
        rightRear.setPower(isPowerReduced ? mecanumWheelSpeeds.rightRearMetersPerSecond * 0.4 / Constants.getMaxSpeedPerSec()
                : mecanumWheelSpeeds.rightRearMetersPerSecond / Constants.getMaxSpeedPerSec());
    }

    public void orientedDrive (double velX, double velY, double omega, double maxSpeed) {
        maxSpeed = Range.clip(maxSpeed, -1, 1);
        double norm = Math.hypot(velX, velY);
//        double vx =  Math.signum(velX) * Math.acos(Math.abs(velX)/norm) * maxSpeed * Constants.getMaxSpeedPerSec();
        double vx =  velX / norm * Constants.getMaxSpeedPerSec();
        double vy =  velY / norm * Constants.getMaxSpeedPerSec();
        MecanumWheelSpeeds mecanumWheelSpeeds = mecanumKinematics.toWheelSpeeds(ChassisSpeeds.fromFieldRelativeSpeeds(
                vy, vx, omega, getHeading()));
        rightFront.setPower(mecanumWheelSpeeds.rightFrontMetersPerSecond / Constants.getMaxSpeedPerSec() * maxSpeed);
        leftFront.setPower(mecanumWheelSpeeds.leftFrontMetersPerSecond / Constants.getMaxSpeedPerSec() * maxSpeed);
        leftRear.setPower(mecanumWheelSpeeds.leftRearMetersPerSecond / Constants.getMaxSpeedPerSec() * maxSpeed);
        rightRear.setPower(mecanumWheelSpeeds.rightRearMetersPerSecond / Constants.getMaxSpeedPerSec() * maxSpeed);
    }

    public void driveStrafe (boolean left, boolean right, boolean isReversed) {
        double motorOutput = 0;
        if (left) motorOutput = -0.35;
        if (right) motorOutput = 0.35;
        if (isReversed) motorOutput = -motorOutput;
        double headingKp = isReversed ? -0.0015 : -0.005;
        double headingCompensate =
                (MyMath.angleWrapForHighSchool(getHeading().getDegrees()) - (isReversed ? 270 : 90)) * headingKp;
        driveMecanum(motorOutput, headingCompensate, 0);
    }

    public void driveStraight(boolean fwd, boolean bkwd, boolean isReversed) {
        double motorOutput = 0;
        if (bkwd) motorOutput = -0.4;
        if (fwd) motorOutput = 0.4;
        if (isReversed) motorOutput = -motorOutput;
        double headingKp = isReversed ? -0.0015 : -0.005;
        double headingCompensate =
                (MyMath.angleWrapForHighSchool(getHeading().getDegrees()) - (isReversed ? 270 : 90)) * headingKp;
        driveTrain.driveMecanum(0, headingCompensate, motorOutput);
    }

    public double getRawYaw(){
        return imu.getAngularOrientation().firstAngle; // in rad from 2 to 2*pi, ONLY on this robot.
    }

    public Rotation2d getHeading () {
        double yawAcc = Math.atan2((float)imu.getAcceleration().xAccel, (float)imu.getAcceleration().yAccel) * 180 / Math.PI;
        double angle = getRawYaw() * 0.98 + yawAcc * 0.02;
        return Rotation2d.fromRadians(angle);
    }

    public double limit(double value) {
        if (value > 1.0) return 1.0;
        return Math.max(value, -1.0);
    }

    private double applySimpleDeadBand(double input) {
        if (Math.abs(input) < 0.15) return 0;
        else return input;
    }

    public void stopMotors () {
        rightFront.setPower(0);
        leftFront.setPower(0);
        leftRear.setPower(0);
        rightRear.setPower(0);
    }

    public void setMotors (double rf, double lf, double lr, double rr) {
        rightFront.setPower(rf);
        leftFront.setPower(lf);
        leftRear.setPower(lr);
        rightRear.setPower(rr);
    }

    public List<Double> getWheelPositions() {
        List<Double> wheelPositions = new ArrayList<>();
        wheelPositions.add(encoderTicksToMeters(rightFront.getCurrentPosition()));
        wheelPositions.add(encoderTicksToMeters(leftFront.getCurrentPosition()));
        wheelPositions.add(encoderTicksToMeters(leftRear.getCurrentPosition()));
        wheelPositions.add(encoderTicksToMeters(rightRear.getCurrentPosition()));
        return wheelPositions;
    }

    public double getAverageWheelPositions() {
        double average = 0;
        for (int i = 0; i < 4; i++) {
             average += getWheelPositions().get(i);
        }
        return average / 4;
    }

    public List<Double> getWheelVelocities() {
        List<Double> wheelVelocities = new ArrayList<>();
        wheelVelocities.add(encoderTicksToMeters(rightFront.getVelocity()));
        wheelVelocities.add(encoderTicksToMeters(leftFront.getVelocity()));
        wheelVelocities.add(encoderTicksToMeters(leftRear.getVelocity()));
        wheelVelocities.add(encoderTicksToMeters(rightRear.getVelocity()));
        return wheelVelocities;
    }
//
//    public ChassisSpeeds getChassisSpeeds () {
//        TrackingWheelSpeeds trackingWheelSpeeds = new TrackingWheelSpeeds(
//                getRightVelocity(),
//                getLeftVelocity(),
//                getHorizontalWheelVelocity()
//        );
//        return ChassisSpeeds.fromFieldRelativeSpeeds(
//                trackingWheelKinematics.toChassisSpeed(trackingWheelSpeeds), getHeading().inverse());
//    }

        public ChassisSpeeds getChassisSpeeds () {
        MecanumWheelSpeeds mecanumWheelSpeeds = new MecanumWheelSpeeds(
                getWheelVelocities().get(0),
                getWheelVelocities().get(1),
                getWheelVelocities().get(2),
                getWheelVelocities().get(3));
        return ChassisSpeeds.fromFieldRelativeSpeeds(
                mecanumKinematics.toChassisSpeed(mecanumWheelSpeeds), getHeading().inverse());
    }

    public double getLeftPosition () {
        return leftTrackingWheel.getVelocity() / 1024 * Math.PI * 3.8;
    }

    public double getRightPosition () {
        return -rightTrackingWheel.getVelocity() / 1024 * Math.PI * 3.8;
    }

    public double getLeftVelocity () {
        return leftTrackingWheel.getVelocity() / 1024 * Math.PI * 3.8;
    }

    public double getRightVelocity () {
        return -rightTrackingWheel.getVelocity() / 1024 * Math.PI * 3.8;
    }

    public double getHorizontalWheelVelocity () {
        return -xPosEncoder.getVelocity() / 1024 * Math.PI * 3.8;
    }

    public double getHorizontalWheelPosition () {
        return - xPosEncoder.getCurrentPosition() * Math.PI * 3.8 / 1024 ;
    }

    public Pose2d updateOdometer() {
        return odometer.updatePose(getHeading(), getChassisSpeeds());
    }

    public double getRightFrontWheel () {
        return getWheelPositions().get(0);
    }

    public boolean getIsFieldRelative () { return isFieldRelative; }

    public String printPose () { return updateOdometer().toString(); }

//    public String printPose2 () { return updateOdometerComplimentary().toString(); }

    public String printChassisSpeed () { return getChassisSpeeds().toString(); }

    public double getRFOutput () { return rightFront.getPower(); }

    public void resetOdometer () {
        Pose2d pose = new Pose2d();
        Rotation2d rot = new Rotation2d();
        odometer.reset(pose, rot);
    }


}