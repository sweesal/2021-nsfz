package org.firstinspires.ftc.teamcode.robot.auto.utilities;

import org.firstinspires.ftc.teamcode.lib.geometry.Pose2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Rotation2d;

public class Pose2dConvert {
    public static Pose2d toCheesyPose (com.acmerobotics.roadrunner.geometry.Pose2d pose) {
        return new Pose2d(
                -pose.getY(), pose.getX(), new Rotation2d(pose.getHeading(), false));
    }

    public static com.acmerobotics.roadrunner.geometry.Pose2d toRRPose (Pose2d pose) {
        return new com.acmerobotics.roadrunner.geometry.Pose2d(
                pose.getTranslation().y(), -pose.getTranslation().x(), pose.getRotation().getDegrees());
    }
}
