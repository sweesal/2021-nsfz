package org.firstinspires.ftc.teamcode.robot.auto.utilities;

import com.qualcomm.robotcore.util.ElapsedTime;

import java.util.concurrent.TimeUnit;

public class AutonomousTimer {
    private static final AutonomousTimer autoTimer= new AutonomousTimer();
    private final ElapsedTime systemTimer;
    public static AutonomousTimer getInstance() {
        return autoTimer;
    }

    private AutonomousTimer () {
        systemTimer = new ElapsedTime();
    }

    public double mark () {
        return systemTimer.now(TimeUnit.MILLISECONDS);
    }

    public ElapsedTime getSystemTimer() {
        return systemTimer;
    }

}
