package org.firstinspires.ftc.teamcode.robot.auto.commands.commandBase;

import java.util.ArrayList;
import java.util.List;

public class ParallelCommand implements ICommand{

    private final ArrayList<ICommand> mCommands;

    public ParallelCommand(List<ICommand> commands) {
        mCommands = new ArrayList<>(commands);
    }

    @Override
    public void init() {
        for (ICommand mCommand : mCommands) {
            mCommand.init();
        }
    }

    @Override
    public void execute() {
        for (ICommand mCommand : mCommands) {
            mCommand.execute();
        }
    }

    @Override
    public void end() {
        for (ICommand mCommand : mCommands) {
            mCommand.end();
        }
    }

    @Override
    public boolean isDone() {
        for (ICommand mCommand : mCommands) if (!mCommand.isDone()) return false;
        return true;
    }


}
