package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.constraints.TrajectoryVelocityConstraint;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;


@Autonomous(name="TestAuto", group="Auto")
//@Disabled
public class AutoBot extends LinearOpMode {

    private final ElapsedTime runtime = new ElapsedTime();
    private Intake intake;
    private Shooter shooter;

    int MAX_SHOOTER = -2400;

    @Override
    public void runOpMode() {
        RobotMap.robotInit(hardwareMap);
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        Intake.setIsIntakeTriggered(false);

        drive.setPoseEstimate(new Pose2d(0, 0, 0));

        Trajectory toSideForGoals = drive.trajectoryBuilder(new Pose2d())
                .splineToSplineHeading(new Pose2d(30, 35, Math.PI), Math.toRadians(0))
                .splineToSplineHeading(new Pose2d(60, 35, Math.PI), 0)
                .addDisplacementMarker(60, () -> {
                    shooter.setShooterVelocity(MAX_SHOOTER, true);
                    shooter.updateSlope(false, false, true, false);
                })
                .build();

        Pose2d pose2dTemp = toSideForGoals.end();

        waitForStart();
        drive.followTrajectory(toSideForGoals);

        //shooter.updateSlope(false, false, true, false);
        intake.updateIntake(true, false);

        for (int i = 0; i < 3; i++) {
            sleep(1000);
            shooter.updateSlope(false, false, true, false);
            shootGoals();
            Trajectory strafeForGoals = drive.trajectoryBuilder(drive.getPoseEstimate())
                    .strafeLeft(7.5)
                    .build();
            drive.followTrajectory(strafeForGoals);
        }

        Trajectory fwdToLine = drive.trajectoryBuilder(drive.getPoseEstimate())
                .back(15)
                .build();
        drive.followTrajectory(fwdToLine);
        drive.turn(Math.PI);

        //sleep(1000);

        Trajectory toMiddleForRings = drive.trajectoryBuilder(drive.getPoseEstimate())
                .splineTo(new Vector2d(40, 5), 0)
                .addDisplacementMarker(1, () -> {
                    intake.updateIntake(true, false);
                })
                .build();

        //drive.followTrajectory(toMiddleForRings);
        sleep(1000);

        Trajectory toMiddleForMoreRings = drive.trajectoryBuilder(drive.getPoseEstimate())
                .splineToConstantHeading(new Vector2d(30, 0), 0)
                .build();

        //drive.followTrajectory(toMiddleForMoreRings);
        intake.updateIntake(false, false);
        shooter.setShooterVelocity(MAX_SHOOTER, true);

    }

    public void shootGoals () {
            shooter.setTriggerRollerOpenLoop(0.7);
            sleep(150);
            shooter.setTriggerRollerOpenLoop(0.3);
            sleep(100);
            shooter.setTriggerRollerOpenLoop(0.5);
    }

}
