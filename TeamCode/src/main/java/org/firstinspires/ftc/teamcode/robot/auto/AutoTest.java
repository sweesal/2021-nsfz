package org.firstinspires.ftc.teamcode.robot.auto;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.auto.config.PIDConfig;
import org.firstinspires.ftc.teamcode.robot.auto.utilities.ControlMethod;
import org.firstinspires.ftc.teamcode.robot.auto.utilities.MotorRamp;
import org.firstinspires.ftc.teamcode.robot.auto.utilities.MyMath;
import org.firstinspires.ftc.teamcode.robot.auto.utilities.Pose2dConvert;
import org.firstinspires.ftc.teamcode.robot.subsystems.DriveTrain;

public class AutoTest {
    private final ElapsedTime autoTimer;
    private AutoState state = AutoState.IDLE;
    private DriveTrain driveTrain = DriveTrain.getInstance();
    private final ControlMethod controlMethod;
    private final static double kHoldingDelayTime = 0.5;
    private final double mCurrentStateStartTime = 0.0;

    private MotorRamp positioningRamp = new MotorRamp(1);

    private double markTime = 0;

    public static final AutoTest autoTest = new AutoTest();

    public static AutoTest getInstance() {
        return autoTest;
    }

    private AutoTest() {
        autoTimer = new ElapsedTime();
        controlMethod = new ControlMethod();
    }

    public enum AutoState {
        IDLE, AIM, TURNING, HOLD;
    }

    public void initAutonomous () {

    }

    public void executeAutonomous () {

    }

    public AutoState getAutoState () {
        return state;
    }
    public String printAutoState () {
        return state.toString();
    }

    public void updateAutoState (double timeStamp, AutoState currentState) {
        synchronized (this) {
            this.state = currentState;
        }

        double timeInState = timeStamp - mCurrentStateStartTime;

        switch (currentState) {
            case IDLE:
                currentState = onHandleIdle();
                break;
            case TURNING:
                currentState = onHandleTurning();
                break;
            case HOLD:
                currentState = onHandleHold(timeStamp);
                break;
            default:
                break;
        }
    }

    private AutoState onHandleIdle () {
        return AutoState.IDLE;
    }

    private AutoState onHandleTurning () {
        return AutoState.IDLE;
    }

    private AutoState onHandleHold (double timeInState) {
        return AutoState.IDLE;
    }

    public void goTurnAngleTo(double targetHeading) {
        PIDConfig pid = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.0035, 0, 0.0),
                0,
                MyMath.angleWrapForPID(driveTrain.getHeading().getDegrees(), targetHeading));

        double motorOutput = Range.clip(pid.output, -1, 1);
        if (Math.abs(pid.error) > 2.5)
            driveTrain.driveMecanum(0, motorOutput, 0);
        else driveTrain.stopMotors();
    }


    public void goTurnAngleOpenLoopTo(double targetHeading) {
        double reading = MyMath.angleWrap(driveTrain.getHeading().getDegrees());
        double error = targetHeading - reading;
        double motorOutput = (error) > 0 ? - 0.3 : 0.3;
        if (error > 3)
            driveTrain.driveMecanum(0, motorOutput, 0);
        else driveTrain.stopMotors();
    }

    public void goForwardFor(double distanceCMeters) {
        MotorRamp positioningRamp = new MotorRamp(1);
        PIDConfig pid = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.0225, 0, 0.001),
                distanceCMeters,
                driveTrain.updateOdometer().getTranslation().y());

        double motorOutput = positioningRamp.applyAsDouble(
                Range.clip(pid.output, -0.5, 0.5), autoTimer.milliseconds());
        double headingKp = 0.005;
        double headingCompensate = 0 - driveTrain.getHeading().getDegrees() * headingKp;
//        if (Math.abs(pid.error) > 15) {
//            driveTrain.driveMecanum(0, headingCompensate, motorOutput);
//        } else if (Math.abs(pid.error) > 5) {
//            driveTrain.driveMecanum(0, headingCompensate, Range.clip(pid.output, -1, 1));
//        } else driveTrain.stopMotors();

        driveTrain.driveMecanum(0, headingCompensate, motorOutput);
    }

    public void goHorizontalFor(double distanceCMeters) {
        PIDConfig pid = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.02, 0, 0),
                distanceCMeters,
                driveTrain.updateOdometer().getTranslation().x());

        double motorOutput = Range.clip(pid.output, -1, 1);
        double headingKp = 0.005;
        double headingCompensate = 0 - driveTrain.getHeading().getDegrees() * headingKp;
        if (Math.abs(pid.error) > 5)
            driveTrain.driveMecanum(motorOutput, headingCompensate, 0);
        else driveTrain.stopMotors();
    }

    public void goDirectionTo(double distanceXCMeters, double distanceYCMeters) {
        PIDConfig pidX = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.016, 0, 0.001),
                distanceXCMeters,
                driveTrain.updateOdometer().getTranslation().x());

        PIDConfig pidY = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.016, 0, 0.001),
                distanceYCMeters,
                driveTrain.updateOdometer().getTranslation().y());

        if (Math.abs(pidY.error) > 5)
            driveTrain.orientedDrive(pidX.output, pidY.output, 0,0.9);
        else driveTrain.stopMotors();
    }

    public void goDirectionWithPoseTo(
            double distanceXCMeters, double distanceYCMeters, double targetPose) {

        PIDConfig pidX = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.007, 0, 0.001),
                distanceXCMeters,
                driveTrain.updateOdometer().getTranslation().x());

        PIDConfig pidY = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.007, 0, 0.001),
                distanceYCMeters,
                driveTrain.updateOdometer().getTranslation().y());

        double angleError = MyMath.angleWrapForPID(
                targetPose, driveTrain.updateOdometer().getRotation().getDegrees());
        double angleOutput = 0;
        if (angleError > 5) angleOutput = 0.025;
        else if (angleError < -5) angleOutput = -0.025;
        else angleOutput = 0;

        if (Math.hypot(pidX.error, pidY.error) > 15 & state == AutoState.IDLE) {
            if (Math.hypot(pidX.error, pidY.error) > 7.5) {
                driveTrain.orientedDrive(pidX.output, pidY.output, 0, 0.9);
            } else {
                driveTrain.orientedDrive(
                        pidX.output*0.75, pidY.output*0.75, 0, 0.9);
            }
        } else {
            state = AutoState.TURNING;
            goTurnAngleTo(targetPose);
        }
    }

    public void goDirectionWithPoseTo(Pose2d currentPose, Pose2d targetPose) {

        org.firstinspires.ftc.teamcode.lib.geometry.Pose2d
                curPose = Pose2dConvert.toCheesyPose(currentPose);
        org.firstinspires.ftc.teamcode.lib.geometry.Pose2d
                tarPose = Pose2dConvert.toCheesyPose(targetPose);

        PIDConfig pidX = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.007, 0, 0.001),
                tarPose.getTranslation().x(),
                curPose.getTranslation().x());

        PIDConfig pidY = controlMethod.applyPIDControl(
                autoTimer.milliseconds(),
                new PIDConfig(0.007, 0, 0.001),
                tarPose.getTranslation().y(),
                curPose.getTranslation().y());

        double angleError = MyMath.angleWrapForPID(
                tarPose.getRotation().getDegrees(), curPose.getRotation().getDegrees());
        double angleOutput = 0;
        if (angleError > 5) angleOutput = 0.025;
        else if (angleError < -5) angleOutput = -0.025;
        else angleOutput = 0;

        if (Math.hypot(pidX.error, pidY.error) > 4 & state == AutoState.IDLE) { // This is in inch.
            if (Math.hypot(pidX.error, pidY.error) > 2) {
                driveTrain.orientedDrive(pidX.output, pidY.output, 0, 0.9);
            } else {
                driveTrain.orientedDrive(
                        pidX.output*0.75, pidY.output*0.75, 0, 0.9);
            }
        } else {
            state = AutoState.TURNING;
            goTurnAngleTo(tarPose.getRotation().getDegrees());
        }
    }

    public void setIdle () {
        state = AutoState.IDLE;
    }

    public String getStateString() {
        return state.toString();
    }


    public void stopMotors () {
        driveTrain.stopMotors();
    }

}
