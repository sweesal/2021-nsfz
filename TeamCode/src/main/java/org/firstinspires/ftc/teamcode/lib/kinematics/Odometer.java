
package org.firstinspires.ftc.teamcode.lib.kinematics;

import android.os.SystemClock;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.lib.geometry.Pose2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Rotation2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Twist2d;

public class Odometer {

    private Pose2d pose;

    private Rotation2d gyroOffset;
    private Rotation2d prevAngle;

    private static double prevTimeStamp = -1;

    private Odometer(
            MecanumKinematics mecanumKinematics, Rotation2d gyroAngle, Pose2d initialPoseMeters) {
        pose = initialPoseMeters;
        this.gyroOffset = pose.getRotation().rotateBy(gyroAngle.inverse());
        this.prevAngle = initialPoseMeters.getRotation();
    }

    public Odometer(
            MecanumKinematics mecanumKinematics, Rotation2d gyroAngle) {
        this(mecanumKinematics, gyroAngle, new Pose2d());
    }

    private Odometer(
            TrackingWheelKinematics trackingWheelKinematics, Rotation2d gyroAngle, Pose2d initialPoseMeters) {
        pose = initialPoseMeters;
        this.gyroOffset = pose.getRotation().rotateBy(gyroAngle.inverse());
        this.prevAngle = initialPoseMeters.getRotation();
    }

    public Odometer(
            TrackingWheelKinematics trackingWheelKinematics, Rotation2d gyroAngle) {
        this(trackingWheelKinematics, gyroAngle, new Pose2d());
    }

    public void reset (Pose2d poseMeters, Rotation2d gyroAngle) {
        pose = poseMeters;
        this.prevAngle = poseMeters.getRotation();
        gyroOffset = pose.getRotation().rotateBy(gyroAngle.inverse());
    }

    public Pose2d getPose () {
        return pose;
    }

    public Pose2d updatePoseWithTime (double currentTimeStamp, Rotation2d gyroAngle, ChassisSpeeds chassisSpeeds) {
        final double dt = prevTimeStamp >= 0 ? currentTimeStamp - prevTimeStamp : 0.0;
        prevTimeStamp = currentTimeStamp;
        Rotation2d angle = gyroAngle.rotateBy(gyroOffset);
        Pose2d newPose = pose.exp(new Twist2d(
                chassisSpeeds.vx * dt / 1000,
                chassisSpeeds.vy * dt / 1000,
                angle.getDegrees() - prevAngle.getDegrees())
        );
        prevAngle = angle;
        pose = new Pose2d(newPose.getTranslation().translateBy(pose.getTranslation()), angle);
        return pose;
    }

    public Pose2d updatePose (Rotation2d gyroAngle, ChassisSpeeds chassisSpeeds) {
        return updatePoseWithTime(SystemClock.uptimeMillis(), gyroAngle, chassisSpeeds);
    }

}
