package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Intake {
    private final DcMotor intake = RobotMap.intake;

    public static void setIsIntakeTriggered(boolean isIntakeTriggered) {
        Intake.isIntakeTriggered = isIntakeTriggered;
    }

    private static boolean isIntakeTriggered = false;

    public Intake() {
        intake.setDirection(DcMotorSimple.Direction.FORWARD);
    }

    private void updateIntake(double setPower) {
        intake.setPower(Range.clip(setPower, -0.99, 0.99));
    }

    public void updateIntake(boolean isBtnPressed, boolean isRevBtnPressed) {
        double intakePower = 0;
        if (isBtnPressed)
            isIntakeTriggered = !isIntakeTriggered;
        if (isIntakeTriggered)
            if (Shooter.isIsShooting())
                intakePower = 0.4;
            else intakePower = 0.9;
        if (isRevBtnPressed) {
            intakePower = -0.9;
            isIntakeTriggered = false;
        }

        updateIntake(intakePower);
    }

}
