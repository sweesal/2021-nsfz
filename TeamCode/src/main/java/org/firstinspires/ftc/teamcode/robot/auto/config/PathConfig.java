package org.firstinspires.ftc.teamcode.robot.auto.config;

public class PathConfig {

    public double maxVel;
    public double maxAcc;
    public double maxAngVel;
    public double spacing;
    public double lookAheadDistance;
    public double targetTolerance;
    public double kS;
    public double kV;
    public double kA;

    public PathConfig () {

    }

    public static PathConfig getPathConfig () {
        PathConfig config = new PathConfig();
        config.maxVel = 90;
        config.maxAcc = 30;
        config.spacing = 10;
        config.maxAngVel = 1;
        config.lookAheadDistance = 20;
        config.targetTolerance = 5;
        config.kS = 0.0;
        config.kV = 0.0;
        config.kA = 0.0;
        return config;
    }


}
