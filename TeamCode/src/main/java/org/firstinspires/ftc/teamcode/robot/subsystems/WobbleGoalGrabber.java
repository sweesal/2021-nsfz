package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class WobbleGoalGrabber {
    private DcMotor ringArm;
    private Servo claw;
    private static boolean isIntakeTriggered = false;

    private final ElapsedTime clawTimer;
    private static double clawPosition = 0.2; //default value

    private static final double intakePower = 0.5; // Test value

    private enum GrabberState {
        IDLE, AIMING, PLACE, HOLD;
    }

    private GrabberState grabberState = GrabberState.IDLE; // Default State.

    public WobbleGoalGrabber() {
        clawTimer = new ElapsedTime();
        ringArm.setDirection(DcMotorSimple.Direction.FORWARD);
        ringArm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        claw.setDirection(Servo.Direction.FORWARD);
    }

    public GrabberState getGrabberState () {
        return grabberState;
    }

    public void setArmOpenLoop(boolean isBtnPressed, boolean isRevBtnPressed) {
        if (isBtnPressed)
            ringArm.setPower(intakePower);//intake power
        else if (isRevBtnPressed)
            ringArm.setPower(-intakePower);
        else
            ringArm.setPower(0);//stop intake
    }

    public void setArmPosition (int degrees) {
        int pos = Range.clip(degrees, 0, 180) / 180 * 1600;
        ringArm.setTargetPosition(pos);
        ringArm.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void resetArm () {
        ringArm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    public void setClaw (boolean cmdUp, boolean cmdDown) {
        if(cmdUp && clawTimer.seconds() > 0.1) {
            clawPosition +=0.05;
            clawTimer.reset();
        } else if(cmdDown && clawTimer.seconds() >0.1) {
            clawPosition -=0.05;
            clawTimer.reset();
        }
        claw.setPosition(Range.clip(clawPosition, 0.01, 0.99)); // Default from 0 - 1.
    }

    private void setGrabberAngle (GrabberState state) {
        synchronized (this) {
            this.grabberState = state;
            switch (state) {
                case IDLE:
                    toVertical();
                    break;
                case AIMING:
                case HOLD:
                    toSideLift();
                    break;
                case PLACE:
                    toHorizontal();
                    break;
            }
        }
    }

    private void setClawState (GrabberState state) {
        synchronized (this) {
            this.grabberState = state;
            switch (state) {
                case IDLE:
                    closeClaw();
                    break;
                case AIMING:
                case HOLD:
                    toSideLift();
                    break;
                case PLACE:
                    openClaw();
                    break;
            }
        }
    }

    public double getArmPosition () {
        return ringArm.getCurrentPosition();
    }

    public double getClawPosition () {
        return claw.getPosition();
    }

    public void aimGoal () {

    }

    public void openClaw () {
        claw.setPosition(0.5);
    }

    public void closeClaw () {
        claw.setPosition(0.01);
    }

    public void toVertical () {
        setArmPosition(90);
    }

    public void toSideLift () {
        setArmPosition(135);
    }

    public void toHorizontal () {
        setArmPosition(180);
    }

    public void toInitialPos () {
        setArmPosition(0);
    }

    private void setIdle () {
        grabberState = GrabberState.IDLE;
    }

    private void setAiming () {
        grabberState = GrabberState.AIMING;
    }

    private void setHold () {
        grabberState = GrabberState.HOLD;
    }

    private void setPlace () {
        grabberState = GrabberState.PLACE;
    }


}
