package org.firstinspires.ftc.teamcode.robot.auto.utilities;

import org.firstinspires.ftc.teamcode.robot.auto.path.Point;

import java.util.ArrayList;

public class MyMath {
    public static double angleWrap (double inputAngle) {
        while (inputAngle >= 360) inputAngle = inputAngle - 360;
        while (inputAngle <= -360) inputAngle = inputAngle + 360;
        if (inputAngle > 180 ) inputAngle = inputAngle - 360;
        if (inputAngle < -180) inputAngle = inputAngle + 360;
        return inputAngle;
    }

    public static double angleWrapForPID (double inputAngle, double targetAngle) {
        double relativeAngle = inputAngle - targetAngle;
        while (relativeAngle >= 360) relativeAngle = relativeAngle - 360;
        while (relativeAngle <= -360) relativeAngle = relativeAngle + 360;
        if (relativeAngle > 180 ) relativeAngle = relativeAngle - 360;
        if (relativeAngle < -180) relativeAngle = relativeAngle + 360;
        return relativeAngle;
    }

    public static double angleWrapForDriving (double inputAngle, double targetAngle) {
        inputAngle = inputAngle + 90;
        targetAngle = targetAngle + 90;
        double relativeAngle = inputAngle - targetAngle;
        while (relativeAngle >= 360) relativeAngle = relativeAngle - 360;
        while (relativeAngle <= -360) relativeAngle = relativeAngle + 360;
        if (relativeAngle > 180 ) relativeAngle = relativeAngle - 360;
        if (relativeAngle < -180) relativeAngle = relativeAngle + 360;
        return relativeAngle;
    }

    public static double angleWrapForHighSchool (double inputAngle) {
        if (inputAngle >= -180 && inputAngle < -90) inputAngle = 450 + inputAngle;
        else if (inputAngle >= -90 && inputAngle < 0) inputAngle += 90;
        else if (inputAngle >= 0 && inputAngle <= 180) inputAngle += 90;
        return inputAngle;
    }

    public static ArrayList<Point> lineCircleIntersection(Point circleCenter, double radius,
                                                          Point linePoint1, Point linePoint2){
        //如果接近垂直，则将交点互相替代
        if(Math.abs(linePoint1.y - linePoint2.y) < 0.002){
            linePoint1.y = linePoint2.y + 0.003;
        }
        if(Math.abs(linePoint1.x - linePoint2.x) < 0.002){
            linePoint1.x = linePoint2.x + 0.003;
        }

        double m1 = (linePoint2.y - linePoint1.y) / (linePoint2.x - linePoint1.x);

        double x1 = linePoint1.x - circleCenter.x;
        double y1 = linePoint1.y - circleCenter.y;

        double quadraticA = 1.0 + Math.pow(m1, 2);
        double quadraticB = (2.0 * m1 * y1) - (2.0 * Math.pow(m1, 2) * x1);
        double quadraticC =
                ((Math.pow(m1, 2) * Math.pow(x1, 2)))
                        - (2.0 * y1 * m1 * x1) + Math.pow(y1, 2) - Math.pow(radius, 2) ;

        ArrayList<Point> allPoints = new ArrayList<>();

        try {
            double xRoot1 = (-quadraticB + Math.sqrt(Math.pow(quadraticB, 2)
                    - (4.0 * quadraticA * quadraticC))) / (2.0 * quadraticA);
            double yRoot1 = m1 * (xRoot1 - x1) + y1;

            xRoot1 += circleCenter.x;
            yRoot1 += circleCenter.y;

            //Box clipping
            double minX = Math.min(linePoint1.x, linePoint2.x);
            double maxX = Math.max(linePoint1.x, linePoint2.x);

            if(xRoot1 > minX && xRoot1 < maxX){
                allPoints.add(new Point(xRoot1, yRoot1));
            }

            double xRoot2 = (-quadraticB - Math.sqrt(Math.pow(quadraticB, 2)
                    + (4.0 * quadraticA * quadraticC))) / (2.0 * quadraticA);
            double yRoot2 = m1 * (xRoot2 - x1) + y1;

            xRoot2 += circleCenter.x;
            yRoot2 += circleCenter.y;

            if(xRoot2 > minX && xRoot2 < maxX){
                allPoints.add(new Point(xRoot2, yRoot2));
            }


        }catch (Exception ignored){

        }
        return allPoints;

    }
}
