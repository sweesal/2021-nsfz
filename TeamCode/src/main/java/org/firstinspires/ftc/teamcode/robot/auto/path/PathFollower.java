package org.firstinspires.ftc.teamcode.robot.auto.path;


import android.os.Build;

import androidx.annotation.RequiresApi;

import org.firstinspires.ftc.teamcode.lib.geometry.Pose2d;
import org.firstinspires.ftc.teamcode.lib.geometry.Vector;
import org.firstinspires.ftc.teamcode.lib.kinematics.ChassisSpeeds;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.auto.config.PathConfig;
import org.firstinspires.ftc.teamcode.robot.subsystems.DriveTrain;

import java.util.ArrayList;
import java.util.Arrays;

public class PathFollower {

    public PathConfig config;
    public ArrayList<Waypoint> path;
    public double pathTotalDistance;
    public boolean onPath, isDone;
    public double progress;

    public Point prevLookAheadPoint;
    public int prevBackClosestWaypointIndex;
    public int prevFrontClosestWaypointIndex;
    public double solidDistanceTravelled;
    public double prevTargetVel;
    public double prevDist2Target;

    public int backClosestWaypointIndex;
    public int frontClosestWaypointIndex;
    public Waypoint robotInterpolatedPos;
    public Point lookAheadPoint;
    public double omega;
    public double dist2Target;
    public double targetVelocity;

    public DriveTrain drive = DriveTrain.getInstance();

    public double angle, dt;
    public Point robotPos;

    double deltaT = 0;
    double prevLeftSpeed = 0;
    double prevRightSpeed = 0;

    double leftSpeed;
    double rightSpeed;
    double leftAccel;
    double rightAccel;
    double leftOutput;
    double rightOutput;
    double prevTimestamp = -1;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void main(String[] args) {
        ArrayList<Waypoint> path = new ArrayList<Waypoint>(Arrays.asList(new Waypoint(1, 4), new Waypoint(400, 4)));
        PathConfig config = new PathConfig();
        path = new PathGenerator().generate(path, config);
        PathFollower pathFollower = new PathFollower(path, config);
        System.out.println(pathFollower.pathTotalDistance);
        pathFollower.path.forEach((waypoint) -> {
            System.out.println("Path " + waypoint);
        });
    }

    public PathFollower(ArrayList<Waypoint> path, PathConfig config) {
        this.config = config;
        this.path = path;
        prevLookAheadPoint = null;
        prevBackClosestWaypointIndex = 0;
        prevFrontClosestWaypointIndex = 0;
        solidDistanceTravelled = 0;
        isDone = false;
        onPath = false;
        progress = 0;
        prevTargetVel = 0;
        prevDist2Target = 99999;
        getPathTotalDistance();
    }

    public void follow (Pose2d pose, double timeStamp) {
        deltaT = timeStamp - prevTimestamp;

        ChassisSpeeds speeds = update(pose, deltaT);

        leftSpeed = speeds.vy + omega;
        rightSpeed = speeds.vy - omega;

        leftAccel = (leftSpeed - prevLeftSpeed) * (1 / dt);
        rightAccel = (rightSpeed - prevRightSpeed) * (1 / dt);

        leftOutput = leftSpeed * config.kV + leftAccel * config.kA;
        rightOutput = rightSpeed * config.kV + rightAccel * config.kA;

        if (leftSpeed > 0.001) {
            leftOutput += config.kS;
        } else if (leftSpeed < -0.001) {
            leftOutput -= config.kS;
        }

        if (rightSpeed > 0.001) {
            rightOutput += config.kS;
        } else if (rightSpeed < -0.001) {
            rightOutput -= config.kS;
        }

        drive.setMotors(rightOutput, leftOutput, leftOutput, rightOutput);

        prevLeftSpeed = leftSpeed;
        prevRightSpeed = rightSpeed;
    }



    public ChassisSpeeds update(Pose2d pose, double dt) {
        this.robotPos = new Point(pose.getTranslation().x(), pose.getTranslation().y());
        this.angle = Math.toRadians(pose.getRotation().getDegrees() + 90);
        this.dt = dt;

        getFinishedPath();
        if (isDone)
            return new ChassisSpeeds(0, 0, 0);

        getClosestWaypoint();
        getInterpolatedWaypoint();
        getProgress();
        getLookAheadPoint();
        getCurvature();

        if (onPath) {
            targetVelocity = Math.min(robotInterpolatedPos.velocity, config.maxAngVel / Math.abs(omega));
        } else {
            targetVelocity = config.maxAngVel / Math.abs(omega);
        }

        double maxDeltaTargetVelocity = config.maxAcc * dt;

        targetVelocity = clamp(targetVelocity, prevTargetVel - maxDeltaTargetVelocity,
                prevTargetVel + maxDeltaTargetVelocity);

        updatePrevVars();

        return new ChassisSpeeds(targetVelocity, 0, omega);
    }

    private void getInterpolatedWaypoint() {
        double d1 = distanceBetween(robotPos, path.get(frontClosestWaypointIndex));
        double d2 = distanceBetween(robotPos, path.get(backClosestWaypointIndex));

        Vector deltaVector = new Vector(path.get(frontClosestWaypointIndex), path.get(backClosestWaypointIndex));
        double d = deltaVector.length();
        double scale = (d * d + d1 * d1 - d2 * d2) / (2 * d);
        Vector xVector = deltaVector.scale(scale / d);
        double ratio = xVector.length() / d;
        if (ratio > 1 || ratio < 0) {
            onPath = false;
            robotInterpolatedPos = path.get(frontClosestWaypointIndex);
            return;
        }
        double vel = path.get(frontClosestWaypointIndex).velocity
                + ratio * (path.get(frontClosestWaypointIndex).velocity - path.get(backClosestWaypointIndex).velocity);

        robotInterpolatedPos = new Waypoint(path.get(frontClosestWaypointIndex).x + xVector.dx,
                path.get(frontClosestWaypointIndex).y + xVector.dy, vel);

        if (distanceBetween(robotInterpolatedPos, robotPos) < config.lookAheadDistance) {
            onPath = true;
        } else {
            onPath = false;
        }
    }

    private void getClosestWaypoint() {
        int searchFrom = prevBackClosestWaypointIndex;
        int searchTo = Math.min(searchFrom + 5, path.size());

        if (!onPath || prevBackClosestWaypointIndex == 0) {
            searchTo = path.size();
        }

        int minIndex = searchFrom;
        double minDistance = Double.MAX_VALUE;

        for (int i = searchFrom; i < searchTo; i++) {
            double distance = distanceBetween(path.get(i), robotPos);
            if (distance < minDistance) {
                minDistance = distance;
                minIndex = i;
            }
        }
        int secondClosestWaypointIndex;
        if (minIndex == 0)
            secondClosestWaypointIndex = minIndex + 1;
        else if (minIndex == path.size() - 1)
            secondClosestWaypointIndex = minIndex - 1;

        if ((distanceBetween(robotPos, path.get(minIndex - 1))) < (distanceBetween(robotPos, path.get(minIndex + 1)))) {
            secondClosestWaypointIndex = minIndex - 1;
        }
        secondClosestWaypointIndex = minIndex + 1;

        frontClosestWaypointIndex = Math.max(minIndex, secondClosestWaypointIndex);
        backClosestWaypointIndex = Math.min(minIndex, secondClosestWaypointIndex);
    }

    private void getLookAheadPoint() {
        int searchFrom = Math.max(backClosestWaypointIndex, 0);
        int searchTo = path.size() - 1;

        for (int i = searchFrom; i < searchTo; i++) {
            Point startOfLine = path.get(i);
            Point endOfLine = path.get(i + 1);
            double radius = config.lookAheadDistance;
            Vector d = new Vector(endOfLine.subtract(startOfLine));
            Vector f = new Vector(startOfLine.subtract(robotPos));

            double a = d.dotProduct(d);
            double b = 2 * f.dotProduct(d);
            double c = f.dotProduct(f) - radius * radius;
            double discriminant = Math.sqrt(b * b - 4 * a * c);
            double t1 = (-b + discriminant) / (2 * a);
            double t2 = (-b - discriminant) / (2 * a);

            boolean t1Found = (t1 >= 0 && t1 <= 1);
            boolean t2Found = (t2 >= 0 && t2 <= 1);
            if (t1Found && !t2Found) {
                Vector v = d.scale(t1);
                lookAheadPoint = new Point(startOfLine.x + v.dx, startOfLine.y + v.dy);
                return;
            } else if (!t1Found && t2Found) {
                Vector v = d.scale(t2);
                lookAheadPoint = new Point(startOfLine.x + v.dx, startOfLine.y + v.dy);
                return;
            } else if (t1Found && t2Found) {
                Vector v1 = d.scale(t1);
                Point lookAheadPoint1 = new Point(startOfLine.x + v1.dx, startOfLine.y + v1.dy);
                Vector v2 = d.scale(t2);
                Point lookAheadPoint2 = new Point(startOfLine.x + v2.dx, startOfLine.y + v2.dy);
                if (distanceBetween(lookAheadPoint1, path.get(frontClosestWaypointIndex)) < distanceBetween(
                        lookAheadPoint2, path.get(frontClosestWaypointIndex))) {
                    lookAheadPoint = lookAheadPoint1;
                } else {
                    lookAheadPoint = lookAheadPoint2;
                }
                return;
            }

        }
        if (prevLookAheadPoint == null) {
            lookAheadPoint = (Point) path.get(frontClosestWaypointIndex);
        }
    }

    public void getCurvature() {

        double gyro = Math.PI - angle;

        double horizontalDistance2LookAheadPoint;
        double a = -Math.tan(gyro);
        double b = 1;
        double c = Math.tan(gyro) * robotPos.x - robotPos.y;

        horizontalDistance2LookAheadPoint = Math.abs(a * lookAheadPoint.x + b * lookAheadPoint.y + c)
                / (Math.sqrt(a * a + b * b));

        double side = Math.signum(
                Math.sin(gyro) * (lookAheadPoint.x - robotPos.x) - Math.cos(gyro) * (lookAheadPoint.y - robotPos.y));

        omega = 2 * side * horizontalDistance2LookAheadPoint / config.lookAheadDistance / config.lookAheadDistance;
    }

    private void getProgress() {
        if (isDone) {
            progress = 100;
            return;
        }
        int addFrom = Math.min(prevBackClosestWaypointIndex, backClosestWaypointIndex);
        int addTo = Math.max(prevBackClosestWaypointIndex, backClosestWaypointIndex);
        int incDirection = (int) Math.signum(backClosestWaypointIndex - prevBackClosestWaypointIndex);
        for (int i = addFrom; i < addTo; i++) {
            solidDistanceTravelled += distanceBetween(path.get(i), path.get(i + 1)) * incDirection;
        }

        double currentDistance = solidDistanceTravelled
                + distanceBetween(path.get(backClosestWaypointIndex), robotInterpolatedPos);

        progress = currentDistance / pathTotalDistance * 100;
    }

    private void getFinishedPath() {
        dist2Target = distanceBetween(robotPos, path.get(path.size() - 1));
        if (dist2Target <= config.targetTolerance) {
            prevDist2Target = dist2Target;
        }
        if (dist2Target > prevDist2Target) {
            isDone = true;
        }
    }

    private void updatePrevVars() {
        prevLookAheadPoint = lookAheadPoint;
        prevBackClosestWaypointIndex = backClosestWaypointIndex;
        prevFrontClosestWaypointIndex = frontClosestWaypointIndex;
        prevTargetVel = targetVelocity;
    }

    private double distanceBetween(Point a, Point b) {
        return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    }

    private void getPathTotalDistance() {
        for (int i = 0; i < path.size() - 1; i++) {
            pathTotalDistance += distanceBetween(path.get(i), path.get(i + 1));
        }
    }

    private double clamp(double x, double min, double max) {
        if (x < min)
            x = min;
        else if (x > max)
            x = max;
        return x;
    }

}
