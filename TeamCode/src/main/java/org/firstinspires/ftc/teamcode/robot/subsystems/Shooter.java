package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Shooter {

    private final DcMotorEx shooterLeft = RobotMap.shooterLeft;
    private final DcMotor shooterRight = RobotMap.shooterRight;

    private final Servo triggerRoller = RobotMap.triggerRoller;
    private final Servo slope = RobotMap.slope;

    private final DigitalChannel switchShoot = RobotMap.switchShoot;
    private final AnalogInput potentiometer = RobotMap.potentiometer;
    private boolean switchLast = false;
    private int ringNum = 0;

    private final ElapsedTime triggerTimer;

    public static void setIsShooting(boolean isShooting) {
        Shooter.isShooting = isShooting;
    }

    public static boolean isIsShooting() {
        return isShooting;
    }

    private static boolean isShooting = false;

    public Shooter() {
        shooterLeft.setDirection(DcMotorSimple.Direction.FORWARD);
        shooterLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        shooterRight.setDirection(DcMotorSimple.Direction.FORWARD);
        shooterRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        switchShoot.setMode(DigitalChannel.Mode.INPUT);
        triggerRoller.setDirection(Servo.Direction.FORWARD);
        triggerTimer = new ElapsedTime();
        initVelocityShooter();
    }

    public enum TriggerRollerState {
        IDLE, RUN, BRAKE
    }

    public enum SlopeState {
        IDLE, TOP, MANUAL, MIDDLE
    }

    public TriggerRollerState getTriggerState() {
        return triggerState;
    }

    public TriggerRollerState triggerState = TriggerRollerState.IDLE;

    public SlopeState getSlopeState() {
        return slopeState;
    }

    public void setSlopeState(SlopeState slopeState) {
        this.slopeState = slopeState;
    }

    public SlopeState slopeState = SlopeState.IDLE;

    public boolean isSwitchPressed (){
        boolean switchPressed;
        switchPressed = getShooterSwitchRaw() && !switchLast;
        switchLast = getShooterSwitchRaw();
        return switchPressed;
    }

    public int ringExceedUpdate () {
        if (isSwitchPressed())
            ringNum++;
        return ringNum;
    }

    public void updateTriggerRoller (boolean btnPressed) {
        switch(triggerState) {
            case IDLE:
                if (btnPressed)
                    triggerState = TriggerRollerState.RUN;
                setTriggerRollerOpenLoop(0.5);
                break;
            case RUN:
                setTriggerRollerOpenLoop(0.7);
                if (!btnPressed) {
                    triggerTimer.reset();
                    triggerState = TriggerRollerState.BRAKE;
                }
                break;
            case BRAKE:
                setTriggerRollerOpenLoop(0.4);
                if (triggerTimer.milliseconds() > 75)
                    triggerState = TriggerRollerState.IDLE;
                break;
        }
    }

    public void updateSlope (boolean cmdUp, boolean cmdDown, boolean setMiddle, boolean setTop) {
        switch (slopeState) {
            case IDLE:
                setSlope(false, false);
                if (cmdUp | cmdDown) slopeState = SlopeState.MANUAL;
                if (setTop) slopeState = SlopeState.TOP;
                if (setMiddle) slopeState = SlopeState.MIDDLE;
                break;
            case TOP:
                setSlope(true, false);
                if (isSlopeAtTop()) {
                    setSlope(false, false);
                    slopeState = SlopeState.IDLE;
                }
                break;
            case MANUAL:
                setSlope(cmdUp, cmdDown);
                if (!cmdUp && !cmdDown) slopeState = SlopeState.IDLE;
                break;
            case MIDDLE:
                if (getSlope() < 0.7) {
                    setSlope(false, true);
                } else if (getSlope() > 0.8) {
                    setSlope(true, false);
                } else {
                    setSlope(false, false);
                    slopeState = SlopeState.IDLE;
                }
                break;
        }
    }

    private void initVelocityShooter () {
        shooterLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        shooterLeft.setVelocityPIDFCoefficients(66, 2.5 ,15, 0);
        shooterRight.getController();
    }

    public void setShooterVelocity (int velocity, boolean isBtnPressed) {
        if(isBtnPressed) {
            isShooting = !isShooting;
        }
        setShooterVelocityForceRun(velocity);
    }

    public void setShooterVelocityForceRun (int velocity) {
        if (isIsShooting()) {
            ShooterRightFeedForward fedFwd = new ShooterRightFeedForward(0, 0, 0.0003);
            shooterLeft.setVelocity(velocity);
            shooterRight.setPower(fedFwd.addFwdlyFeedBack(getShooterVelocity(), velocity, 0.0009));
        } else {
            shooterLeft.setVelocity(0);
            shooterRight.setPower(0);
        }
    }


    static class ShooterRightFeedForward {
        double ks;
        double ka;
        double kv;

        public ShooterRightFeedForward (double ks, double ka, double kv) {
            this.ks = ks;
            this.ka = ka;
            this.kv = kv;
        }

        public double calculate (double velocity, double acceleration) {
            return ks * Math.signum(velocity) + kv * velocity + ka * acceleration;
        }

        public double calculate(double velocity) {
            return calculate(velocity, 0);
        }

        public double addFwdlyFeedBack (double curVelocity, double tarVelocity, double kP) {
            double err = tarVelocity - curVelocity;
            return Range.clip(kv * tarVelocity + err * kP, -1, 1);
        }

    }


    public void setShooterOpenLoop(boolean isBtnPressed) {
        if(isBtnPressed) {
            isShooting = !isShooting;
        }
        if(isShooting) {
            shooterLeft.setPower(-0.99);
            shooterRight.setPower(-0.99);
        }
        else {

            shooterRight.setPower(0);
            shooterLeft.setPower(0);
        };
    }

    public boolean getShooterSwitchRaw() {
        return switchShoot.getState();
    }

    public boolean isSlopeAtTop () { return getSlope() < 0.4; }
    public boolean isSlopeAtButton () { return getSlope() > 1.15; }

    public void setSlope (double input) {
        slope.setPosition(Range.clip(input, 0.1, 0.9));
    }

    public void setSlope (boolean cmdUp, boolean cmdDown) {
        if(!isSlopeAtTop() & cmdUp) {
            setSlope(0.3);
        } else if(!isSlopeAtButton() & cmdDown) {
            setSlope(0.7);
        } else
            setSlope(0.5);
    }

    // Set trigger.
    public void setTriggerRoller (boolean btnSet, boolean btnShootOnce) {
        double triggerRollerPower = 0.5;
        if (btnSet) triggerRollerPower = 0.7;
        if (btnShootOnce) triggerRollerPower = 0.6;
        setTriggerRollerOpenLoop(triggerRollerPower);
    }

    public void setTriggerRollerOpenLoop (double setSpeed) {
        triggerRoller.setPosition(Range.clip(setSpeed, 0, 1));
    }

    public double getSlope () {
        return potentiometer.getVoltage(); //0.1.-1.56
    }

    public double getShooterVelocity() {
        return shooterLeft.getVelocity();
    }

    public double getTriggerRoller () {
        return triggerRoller.getPosition();
    }

}
