/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.qualcomm.ftcrobotcontroller;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.qualcomm.ftcrobotcontroller";
  public static final String BUILD_TYPE = "debug";
  // Field from default config.
  public static final String BUILD_TIME = "2021-04-09T20:52:17.734+0800";
}
